import { createStore } from "vuex";
import users from "./users";

const store = createStore({
  modules: {
    users: users,
  },
});

export default store;
